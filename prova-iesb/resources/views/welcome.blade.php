<!DOCTYPE html>
<html>
<head>
    @vite('resources/css/app.css')
</head>
<body>
<div id="app"></div>
    @vite('resources/js/app.js')
<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
</body>
</html>
