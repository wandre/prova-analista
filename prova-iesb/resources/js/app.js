import './bootstrap';
import {createApp} from 'vue'

import App from './App.vue'
import Router from './router.vue'


createApp(App,Router).mount("#app");
