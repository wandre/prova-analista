<?php

namespace Database\Seeders;

use App\Models\Aluno;
use App\Models\Curso;
use App\Models\Endereco;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;

class CursoAlunoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = FakerFactory::create();

        // Gerar dados fictícios para Cursos
        for ($i = 1; $i <= 10; $i++) {
            Curso::create([
                'codigo_curso' => $faker->unique()->bothify('C???'),
                'nome' => $faker->sentence(3),
            ]);
        }

        // Gerar dados fictícios para Alunos
        for ($i = 1; $i <= 100; $i++) {
          $aluno = Aluno::create([
                'matricula' => $faker->unique()->bothify('A?????'),
                'nome' => $faker->name,
                'curso_id' => $faker->numberBetween(1, 10), // Relacionado a um dos cursos
            ]);

            Endereco::create([
                'rua' => $faker->streetAddress,
                'cidade' => $faker->city,
                'aluno_id' => $aluno->id,
                'estado' => $faker->country,
                'cep' => $faker->countryCode,
            ]);
        }
    }
}
