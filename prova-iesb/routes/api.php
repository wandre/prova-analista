<?php

use App\Http\Controllers\AlunoController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\EnderecoController;
use App\Models\Endereco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// Rotas para Cursos
Route::resource('cursos', CursoController::class);
Route::resource('endereco', EnderecoController::class);

// Rotas para Alunos
Route::resource('alunos', AlunoController::class);

//// Rotas para Alunos
//Route::get('alunos', [AlunoController::class, 'index']);
//Route::get('alunos/{id}', [AlunoController::class, 'show']);
//Route::post('alunos', [AlunoController::class, 'store']);
//Route::put('alunos/{id}', [AlunoController::class, 'update']);
//Route::delete('alunos/{id}', [AlunoController::class, 'destroy']);
Route::get('alunos-search', [AlunoController::class, 'search']);

//// Rotas para Cursos
//Route::get('cursos', [CursoController::class, 'index']);
//Route::get('cursos/{id}', [CursoController::class, 'show']);
//Route::post('cursos', [CursoController::class, 'store']);
//Route::put('cursos/{id}', [CursoController::class, 'update']);
//Route::delete('cursos/{id}', [CursoController::class, 'destroy']);
Route::get('cursos-search', [CursoController::class, 'search']);
Route::get('cursos-qtdalunosporcurso', [CursoController::class, 'qtdAlunosPorCurso']);
Route::get('cursos-alunosporcurso', [CursoController::class, 'AlunosPorCurso']);
Route::get('buscarcep/{cep}', [EnderecoController::class, 'buscarCep']);
