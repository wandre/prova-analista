<?php

namespace App\Http\Controllers;

use App\Models\Endereco;
use App\Services\CepService;
use App\Services\FiltroCursoService;
use App\Services\JsonResponseService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class EnderecoController extends Controller
{

    protected $jsonResponseService;
    protected $filtroService;
    protected $cepService;

    public function __construct(JsonResponseService $jsonResponseService, FiltroCursoService $filtroService, CepService $cepService)
    {
        $this->cepService = $cepService;
        $this->jsonResponseService = $jsonResponseService;
        $this->filtroService = $filtroService;
    }

    public function buscarCep($cep)
    {
        $resultado = $this->cepService->buscarCep($cep);

        if ($resultado) {
            return $this->jsonResponseService->success($resultado, 'Endereço recuperado');
        } else {
            return $this->jsonResponseService->error('Cep não encontrado.', $data = null);
        }
    }

    public function search(Request $request)
    {
        try {
            $query = Endereco::query();
            $query = $this->filtroService->aplicarFiltros($query, $request->all());
            $resu = $query->get();
            return $this->jsonResponseService->success($resu, 'Resultado recuperado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar os enderecos.', $e);
        }
    }


    public function index(Request $request)
    {
        try {
            $query = Endereco::query();
            $resu = $query->paginate(5);
            return $this->jsonResponseService->success($resu, 'Enderecos recuperados com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar os enderecos.', $e);
        }
    }

    public function store(Request $request)
    {
        try {
            $endereco = Endereco::create($request->all());
            return $this->jsonResponseService->success($endereco, 'Dado criado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao salvar.', $e);
        }
    }

    public function show($id)
    {
        try {
            $endereco = Endereco::with('curso','endereco')->find($id);
            return $this->jsonResponseService->success($endereco, 'Dado recuperado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro.', $e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $endereco = Endereco::find($id);
            $endereco->update($request->all());
            return $this->jsonResponseService->success($endereco, 'Dado atualizado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro.', $e);
        }
    }

    public function destroy($id)
    {
        try {
             $endereco = Endereco::destroy($id);
            return $this->jsonResponseService->success( $endereco,'Dado deletado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro.', $e);
        }
    }

}
