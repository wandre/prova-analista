<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use App\Services\FiltroCursoService;
use App\Services\JsonResponseService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CursoController extends Controller
{

    protected $jsonResponseService;
    protected $filtroService;

    public function __construct(JsonResponseService $jsonResponseService, FiltroCursoService $filtroService)
    {
        $this->jsonResponseService = $jsonResponseService;
        $this->filtroService = $filtroService;
    }

    public function search(Request $request)
    {
        try {
            $query = Curso::query();
            $query = $this->filtroService->aplicarFiltros($query, $request->all());

            return $this->jsonResponseService->success($query, 'Resultado recuperado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar os cursos.', $e);
        }
    }

    public function index(Request $request)
    {
        try {

            $query = Curso::query();

            $cursos = $this->filtroService->aplicarFiltros($query, $request->all());

            return $this->jsonResponseService->success($cursos, 'Cursos recuperados com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar os cursos.', $e);
        }

    }

    public function store(Request $request)
    {
        try {
            $dados = $request->all();

            if(isset($dados['id'])){
                $curso = Curso::find($dados['id']);
                $curso->update($request->all());
                $msg = 'Curso Atualizado';
            }else{
                $curso = Curso::create($request->all());
                $msg = 'Curso Criado';
            }

            return $this->jsonResponseService->success($curso, $msg);
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar os cursos.', $e);
        }
    }

    public function show($id)
    {
        try {
            $curso = Curso::find($id);
            return $this->jsonResponseService->success($curso, 'Curso recuperado com sucesso');

        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar o curso.', $e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $curso = Curso::find($id);
            $curso->update($request->all());
            return $this->jsonResponseService->success($curso, 'Curso atualizado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar o curso.', $e);
        }
    }

    public function destroy($id)
    {
        try {
            Curso::destroy($id);
            return $this->jsonResponseService->success(null, 'Curso deletado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar o curso.', $e);
        }
    }

    public function qtdAlunosPorCurso()
    {
        try {
            $cursos = Curso::withCount('alunos')->get();

            $relatorio = [];
            foreach ($cursos as $curso) {
                $relatorio[] = [
                    'curso' => $curso->nome,
                    'quantidade_alunos' => $curso->alunos_count,
                ];
            }
            return $this->jsonResponseService->success($relatorio, 'Dados recuperados com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar os dados.', $e);
        }
    }

    public function AlunosPorCurso()
    {
        try {
            $cursos = Curso::with('alunos')->get();

            $relatorio = [];

            foreach ($cursos as $curso) {
                $cursoInfo = [
                    'curso' => $curso->nome,
                    'alunos' => $curso->alunos->sortBy('nome')->values()->all(),
                ];

                $relatorio[] = $cursoInfo;
            }

            return $this->jsonResponseService->success($relatorio, 'Dados recuperados com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar os dados.', $e);
        }
    }


}
