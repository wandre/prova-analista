<?php

namespace App\Http\Controllers;

use App\Models\Aluno;
use App\Models\Curso;
use App\Models\Endereco;
use App\Services\FiltroAlunoService;
use App\Services\JsonResponseService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class AlunoController extends Controller
{

    protected $jsonResponseService;
    protected $filtroService;

    public function __construct(JsonResponseService $jsonResponseService, FiltroAlunoService $filtroService)
    {
        $this->jsonResponseService = $jsonResponseService;
        $this->filtroService = $filtroService;
    }


    public function search(Request $request)
    {
        try {
            $query = Aluno::query();
            $query = $this->filtroService->aplicarFiltros($query, $request->all());
            $resu = $query->get();
            return $this->jsonResponseService->success($resu, 'Resultado recuperado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar os alunos.', $e);
        }
    }


    public function index(Request $request)
    {
        try {
            $query = Aluno::query();
            $query->with('curso','endereco');
            $resu = $this->filtroService->aplicarFiltros($query, $request->all());


            return $this->jsonResponseService->success($resu, 'Alunos recuperados com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao recuperar os alunos.', $e);
        }
    }

    public function store(Request $request)
    {
        try {


            $dados = $request->all();

            if(isset($dados['id'])){
                $aluno = Aluno::find($dados['id']);
                $aluno->update($request->all());
                $msg = 'Aluno Atualizado';
            }else{
                $aluno = Aluno::create($request->all());
                $msg = 'Aluno Criado';
            }


            // Verifica se o aluno possui um endereço associado
            if ($aluno->endereco) {
                $endereco = $aluno->endereco;
            } else {
                $endereco = new Endereco();
            }

            // Atualiza os dados do endereço
            $endereco->rua = $request->input('rua');
            $endereco->cidade = $request->input('cidade');
            $endereco->estado = $request->input('estado');
            $endereco->cep = $request->input('cep');

            // Associa o endereço ao aluno
            $aluno->endereco()->save($endereco);

            return $this->jsonResponseService->success($aluno, $msg);
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro ao salvar.', $e);
        }
    }

    public function show($id)
    {
        try {
            $aluno = Aluno::with('curso','endereco')->find($id);
            return $this->jsonResponseService->success($aluno, 'Aluno recuperado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro.', $e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $aluno = Aluno::find($id);
            $aluno->update($request->all());
            return $this->jsonResponseService->success($aluno, 'Aluno atualizado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro.', $e);
        }
    }

    public function destroy($id)
    {
        try {
             $aluno = Aluno::destroy($id);
            return $this->jsonResponseService->success( $aluno,'Aluno deletado com sucesso');
        } catch (QueryException $e) {
            return $this->jsonResponseService->error('Ocorreu um erro.', $e);
        }
    }

}
