<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aluno extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['matricula', 'nome', 'curso_id'];
    protected $dates = ['deleted_at'];

    public function curso()
    {
        return $this->belongsTo(Curso::class);
    }

    public function endereco()
    {
        return $this->hasOne(Endereco::class);
    }
}
