<?php

namespace App\Services;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class FiltroCursoService
{
    public function aplicarFiltros(Builder $query, array $request)
    {
        $filtros = $request;
        $pagina = 1;
        $registrosPorPagina = 5;
        if(isset($request['page'])){
            $pagina = $request['page'];
            unset($request['pagina']);
        }

        if(isset($request['limit'])){
            $registrosPorPagina = $request['limit'];
            unset($request['limit']);
        }

        foreach ($filtros as $campo => $valor) {
            if ($valor !== null) {
                if($valor !== "null"){
                    if(!isset($filtros['page']) ){
                        $query->orWhere('nome', 'LIKE', '%' . $valor . '%');
                        $query->orWhere('codigo_curso', 'LIKE', '%' . $valor . '%');
                    }
                }
            }
        }

        // Aplicar a paginação
        $total = $query->count();
        $query->skip(($pagina - 1) * $registrosPorPagina)->take($registrosPorPagina);

        $resultados = $query->get();

        return new LengthAwarePaginator($resultados, $total, $registrosPorPagina, $pagina);
    }

}
