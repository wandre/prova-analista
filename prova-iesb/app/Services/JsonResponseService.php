<?php

namespace App\Services;

class JsonResponseService
{
    public function success($data = null, $message = null, $status = 200)
    {
        $response = [
            'data' => $data,
            'message' => $message,
            'result' => true,
        ];

        return response()->json($response, $status);
    }

    public function error($message,$erroInterno, $status = 400)
    {
        $response = [
            'data' => false,
            'message' => $message,
            'result' => false,
            'erroInterno' => $erroInterno
        ];

        return response()->json($response, $status);
    }
}
