<?php

namespace App\Services;

use GuzzleHttp\Client;

class CepService
{
    public function buscarCep($cep)
    {

        if($cep >= 8){
            $client = new Client();
            $url = "https://viacep.com.br/ws/{$cep}/json";
            $response = $client->get($url);

            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody(), true);
                return $data;
            }
        }

        return null;
    }
}
