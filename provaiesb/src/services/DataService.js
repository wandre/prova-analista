import http from "../http-common";

class DataService {

    // CURSOS
    getAllCursos(pagina) {
        if(pagina){
            return http.get(`/cursos?page=${pagina}`);
        }else{
            return http.get(`/cursos`);
        }
    }

    getListCursos() {
        return http.get(`/cursos?page=1&limit=999`);
    }

    findyNomeCurso(nome) {
        return http.get(`/cursos?nome=${nome}`);
    }

    createCurso(data) {
        return http.post("/cursos", data);
    }

    getCurso(id) {
        return http.get(`/cursos/${id}`);
    }

    deleteCurso(id) {
        return http.delete(`/cursos/${id}`);
    }

    getQtdalunosporcurso() {
        return http.get(`/cursos-qtdalunosporcurso`);
    }

    getAlunosporcurso() {
        return http.get(`/cursos-alunosporcurso`);
    }


    //ALUNOS
    getAllAlunos(pagina) {
        if(pagina){
            return http.get(`/alunos?page=${pagina}`);
        }else{
            return http.get(`/alunos`);
        }
    }

    findyNomeAluno(nome) {
        return http.get(`/alunos?nome=${nome}`);
    }

    createAluno(data) {
        return http.post("/alunos", data);
    }

    getAluno(id) {
        return http.get(`/alunos/${id}`);
    }

    deleteAluno(id) {
        return http.delete(`/alunos/${id}`);
    }

    buscarcep(cep) {
        return http.get(`/buscarcep/${cep}`);
    }

}

export default new DataService();