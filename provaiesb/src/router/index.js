import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      alias: "/cursos",
      name: "cursos",
      component: () => import("../components/curso/CursosList.vue")
    },
    {
      path: "/cursos/adicionar",
      alias: "/cursos/adicionar",
      name: "cursosadicionar",
      component: () => import("../components/curso/AddCurso.vue")
    },
    {
      path: "/cursos/editar/:id",
      alias: "/cursos/editar",
      name: "cursoseditar",
      component: () => import("../components/curso/AddCurso.vue")
    },

    {
      path: "/alunos",
      name: "alunos-details",
      component: () => import("../components/aluno/AlunosList.vue")
    },
    {
      path: "/alunos/editar/:id",
      alias: "/alunos/editar",
      name: "alunoseditar",
      component: () => import("../components/aluno/AddAluno.vue")
    },
    {
      path: "/aluno/adicionar",
      alias: "/aluno/adicionar",
      name: "alunosadicionar",
      component: () => import("../components/aluno/AddAluno.vue")
    },

    {
      path: "/relatorio",
      alias: "/relatorio",
      name: "relatoriocurso",
      component: () => import("../components/relatorio/Lista.vue")
    },
  ]
})

export default router
